package shotuploader

import (
	"errors"
	"fmt"
	"github.com/atotto/clipboard"
	"github.com/getlantern/systray"
	"github.com/martinlindhe/notify"
	"github.com/minio/minio-go"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

type S3Handler struct {
	config    ConfigMap
	client    *minio.Client
	appConfig *AppConfig
}

func (h *S3Handler) Handle(file string) error {
	log.Println("starting upload file: ", file)
	systray.SetIcon(h.appConfig.BlueIconData)
	fileName := filepath.Base(file)
	objectName := fmt.Sprintf("%s/%s", h.config["basePath"], fileName)
	UrlSafeName := fmt.Sprintf("%s/%s", h.config["basePath"], url.PathEscape(fileName))
	f, err := os.Open(file)
	if err != nil {
		return errors.New("open file error: " + err.Error())
	}
	contentType := GetFileContentType(f)
	uploadedSize, err := h.client.FPutObject(
		h.config["bucket"].(string),
		objectName,
		file,
		minio.PutObjectOptions{
			ContentType: contentType,
			UserMetadata: map[string]string{
				"agent":     "shotuploader",
				"x-amz-acl": "public-read",
			},
		},
	)
	if err != nil {
		log.Println("s3 > failed to upload file: ", err)
		return err
	}
	log.Println("s3 > uploaded size: ", uploadedSize)
	// get url
	uploadedUrl := fmt.Sprintf("%s/%s", h.config["baseUrl"], UrlSafeName)

	log.Println("s3 > upload url: ", uploadedUrl)
	_ = clipboard.WriteAll(uploadedUrl)

	notify.Notify("shotuploader", "alert", "uploaded: "+uploadedUrl, "")
	systray.SetIcon(h.appConfig.WhiteIconData)

	return nil
}

func NewS3Handler(config ConfigMap, appCfg *AppConfig) (error, Handler) {
	if err := validateConfig(config); err != nil {
		return err, nil
	}

	h := &S3Handler{config: config, appConfig: appCfg}
	err := h.checkBucket()

	return err, h
}

func validateConfig(config ConfigMap) error {
	requireKeys := []string{"endpoint", "accessKey", "secret", "bucket", "basePath"}
	var missingKeys []string
	for _, k := range requireKeys {
		v, ok := config[k]
		if !ok || v == "" {
			missingKeys = append(missingKeys, k)
		} else {

		}
	}
	if len(missingKeys) > 0 {
		return errors.New("missing keys: " + strings.Join(missingKeys, ", "))
	}

	config["basePath"] = strings.Trim(config["basePath"].(string), "/")

	return nil
}

func (h *S3Handler) checkBucket() error {
	client, err := minio.New(
		h.config["endpoint"].(string),
		h.config["accessKey"].(string),
		h.config["secret"].(string),
		true, // use ssl
	)
	if err != nil {
		return err
	}
	h.client = client
	bucketName := h.config["bucket"].(string)
	exists, err := client.BucketExists(bucketName)
	if err != nil {
		return err
	}

	if !exists {
		return errors.New("s3 > bucket doesn't exists: " + bucketName)
	}
	log.Println("s3 > check bucket success")
	return nil
}
