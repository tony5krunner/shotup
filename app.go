package shotuploader

import (
	"log"
)

var initializers = map[string]InitFunc{
	"stdout": NewStdoutHandler,
	"http":   NewHttpHandler,
	"s3":     NewS3Handler,
}

type Handler interface {
	Handle(file string) error
}

type InitFunc func(cfg ConfigMap, config *AppConfig) (error, Handler)

type ConfigMap map[string]interface{}

type AppConfig struct {
	Handlers      []ConfigMap `yaml:"handlers"`
	WatchDir      string      `yaml:"watch_dir"`
	BlueIconData  []byte
	WhiteIconData []byte
}

type App struct {
	config   *AppConfig
	handlers []Handler
}

func NewApp(config *AppConfig) (error, *App) {
	app := &App{
		config: config,
	}

	err := app.initHandlers()

	return err, app
}

func (app *App) initHandlers() (err error) {
	for _, cfg := range app.config.Handlers {
		_type := cfg["type"].(string)
		fnInit, ok := initializers[_type]
		if !ok {
			continue
		}
		err, handler := fnInit(cfg, app.config)
		if err != nil {
			log.Printf("failed to init handler: [%s] %s \n", _type, err.Error())
			continue
		}
		log.Printf("init handler success: %s \n", _type)
		app.handlers = append(app.handlers, handler)
	}
	return
}

func (app *App) Handle(file string) {
	for _, handler := range app.handlers {
		err := handler.Handle(file)
		if err != nil {
			log.Printf("failed to handle: %s \n", err)
		}
	}
}
